function data2save = singleAsset2csv(stock)
    % Author Zuli HUANG
    % Function : Convert a single asset time series into csv format
    % 
    [tmp, numAsset] = size(stock);
    assert(numAsset == 1);
    [T, tmp] = size(stock.AdjClose);

    ticker = stock.Ticker;
    rowNames = fieldnames(stock);
    rowNames = rowNames(~strcmp(rowNames, 'Ticker'));
    data2save = rmfield(stock, 'Ticker');
    data2save = struct2table(data2save);
end
