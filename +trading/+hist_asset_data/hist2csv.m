function data2save = hist2csv(stocks)
    % Author Zuli HUANG
    % Function : Convert array financil time series into matlab table format
    % 
    [tmp, numAsset] = size(stocks);
    assert(numAsset >= 1);
    [T, tmp] = size(stocks(1).AdjClose);
    assetPX = NaN(T, numAsset);
    tickers = {};
    for kk = 1:numAsset
        assetPX(:, kk) = stocks(kk).AdjClose;
        tickers{end+1} = stocks(kk).Ticker;
    end
    data2save = array2table(assetPX);
    data2save.Properties.VariableNames = tickers;
    data2save.Properties.RowNames = stocks(1).Date;
end