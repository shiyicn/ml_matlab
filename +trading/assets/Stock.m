classdef Stocks < Assets
   properties
      NumShares double = 0
      Symbol string
   end
   properties (SetAccess = private)
      Type = "Stocks"
   end
   properties (Dependent)
      SharePrice double
   end
   methods
      function sk = Stocks(description,numshares,symbol)
         if nargin == 0
            description = '';
            numshares = 0;
            symbol = '';
         end
         sk.Description = description;
         sk.NumShares = numshares;
         sk.Symbol = symbol;
      end
      function value = getCurrentValue(sk)
         value = sk.NumShares*sk.SharePrice;
      end
      function pps = get.SharePrice(sk)
         % Implement web access to obtain
         % Current price per share
         % Returning dummy value
         pps = 1;
      end
   end
end