classdef  Assets < matlab.mixin.Heterogeneous
   % file: +financial.@Assets/Assets.m
   properties
      Description char = 'Assets'
   end
   properties (Abstract, SetAccess = private)
      Type
   end
   methods (Abstract)
      % Not implemented by Assets class
      value = getCurrentValue(obj)
   end
   methods (Static, Sealed, Access = protected)
      function defaultObject = getDefaultScalarElement
         defaultObject = financial.DefaultAsset;
      end
   end
   methods (Sealed)
      % Implemented in separate files
      % +financial.@Assets/pie.m
      % +financial.@Assets/makeReport.m
      pie(assetArray)
      makeReport(assetArray)
   end
end