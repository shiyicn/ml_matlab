function c = correlation(S)
    % correlation - Description
    %
    % Syntax: c = correlation(S)
    %
    % Long description
    prpi(S);
    if isstruct(S)
        X = struct2array(S);
    else
        X = S;
        prpi(X);
    end
    c = corr(X);
    varNames = fieldnames(S);
    c = array2table(c,'RowNames', varNames, 'VariableNames', varNames);
end
