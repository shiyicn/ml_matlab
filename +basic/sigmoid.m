function s = sigmoid(X)
    %{
    ================================================================
    Function: 
    ---------
    sigma(X) = 1 / (1 + exp(-X))

    Args: 
    -----
    X : input matrix

    Example:
    --------
    >> X = rand(100, 30);
    >> s = sigmoid(X)
    
    Returns:
    --------
    s : matrix-like output 
    ================================================================
    %}
    s = 1 ./ (1 + exp(-X));
end
