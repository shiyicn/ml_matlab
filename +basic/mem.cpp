#include "mex.h"
#include <stdio.h>

#define A_IN prhs[0]

using namespace std;

void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[]) {
    double *A;
    A = mxGetPr(A_IN);
    /* print the address of the input */
    printf("Address of Input Variable : 0x%p\n", A);
    return;
}