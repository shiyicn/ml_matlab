#include "mex.h"
#include <stdio.h>
#include <iostream>
#include <string>
#include <random>

using namespace std;

void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[]) {
    /**
     * A problem here : 
     * It seems that the pseudo random numbers has always the same seed
     * 
     */

    #define MU_IN prhs[0]
    #define SIGMA_IN prhs[1]
    #define N_IN prhs[2]

    #define X_OUT plhs[0]
    
    double mu = *mxGetPr(MU_IN);
    double sigma = *mxGetPr(SIGMA_IN);
    int N = (int) *mxGetPr(N_IN);

    X_OUT = mxCreateDoubleMatrix(1, N, mxREAL); /* Create the output matrix */
    double* X = mxGetPr(X_OUT);

    // Create normal distribution generator
    default_random_engine generator;
    normal_distribution<double> distribution(mu, sigma);
    
    X[0] = 0;
    for (int i=1; i<N; i++) {
        X[i] = X[i-1] + distribution(generator);
    }
}
