N = 100; % num of data points

[X1, X2] = meshgrid(linspace(0, 5, N), linspace(0, 5, N));
X = [X1(:), X2(:)]; % construct multi-dimension array
w = [1; 1];
y = sin(X * w);
y_noised = y + normrnd(0, 0.2, size(y));

% partition the data into 2 part
perm = randperm(N);
splitRatio = 0.8;
split = floor(N * splitRatio);
permTrain = perm(1:split);
permVal = perm((split+1):N);
X_train = X(permTrain, :);
y_train = y_noised(permTrain, :);
X_val = X(permVal, :);
y_val = y_noised(permVal, :);

