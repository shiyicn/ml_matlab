N = 10000;

tic
X = rand(N, N);
toc

tic
aux = @(v) sum(v .* v);
pattern_X = cellfun(aux, num2cell(X, 2));
toc

tic
pattern_X = sum(X.^2, 2);
toc

tic
pattern_X = sum(X.*X, 2);
toc
