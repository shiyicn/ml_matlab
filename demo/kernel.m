addpath('~/proj/ml_matlab/')

N = 1000;

X = linspace(0, 5, N)';
y = sin(X);
y_noised = y + normrnd(0, 0.2, size(y));

% partition the data into 2 part
perm = randperm(N);
splitRatio = 0.8;
split = floor(N * splitRatio);
permTrain = perm(1:split);
permVal = perm((split+1):N);
X_train = X(permTrain, :);
y_train = y_noised(permTrain, :);
X_val = X(permVal, :);
y_val = y_noised(permVal, :);

sigma = 1.;
pen = 0.01;
K_train = kernel.kers.gaussian_kernel(X_train, sigma);
a = kernel.ridge.ridge_regression(K_train, y_train, pen);
y_predicted_train = K_train * a;

K_val = kernel.kers.gaussian_kernel(X_train, sigma, 'new_input', X_val);
y_predicted_val = K_val * a;

% visualization with predicted values
scatter(X_train, y_train);
hold on;
scatter(X_train, y_predicted_train);
hold off;

% visualization with predicted values
scatter(X_val, y_val);
hold on;
scatter(X_val, y_predicted_val);
hold off;

% Test the Kernel Logistic Regression
addpath('~/proj/ml_matlab')

% Load Diabet dataset
dataset_path = '~/proj/data/diabetes.csv';
input_data = readtable(dataset_path);
input_data = table2array(input_data);
X = input_data(:, 1:8);
y = input_data(:, 9);

N = size(X, 1);

% partition the data into 2 part
perm = randperm(N);
splitRatio = 0.8;
split = floor(N * splitRatio);
permTrain = perm(1:split);
permVal = perm((split+1):N);
X_train = X(permTrain, :);
y_train = y(permTrain, :);
% preprocessing with normalisation method
[X_train, X_mean, X_std] = preprocessing.normalize(X_train);

X_val = X(permVal, :);
y_val = y(permVal, :);

X_val = preprocessing.normalize(X_val, 'mean', X_mean, 'std', X_std);

sigma = 1.;
pen = 0.01;
K_train = kernel.kers.gaussian_kernel(X_train, sigma);
a = kernel.logistic.logistic_regression(K_train, y_train, pen);
y_predicted_train = K_train * a;
y_predicted_train = int8(y_predicted_train >= 0);

K_val = kernel.kers.gaussian_kernel(X_train, sigma, 'new_input', X_val);
y_predicted_val = K_val * a;
y_predicted_val = int8(y_predicted_val >= 0);
