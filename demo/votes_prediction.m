if ispc
    addpath('C:\\Users\\Huang Zuli\\proj\\ml_matlab')
else
    addpath('~/proj/ml_matlab/')
end

if ispc
    dataRootPath = 'C:\\Users\\Huang Zuli\\proj\\data\\votes';
else
    dataRootPath = '~/proj/data/votes/';
end

if ispc
    outputPath = 'C:\\Users\\Huang Zuli\\proj\\data\\votes\\demo_results';
else
    outputPath = '~/proj/data/votes/demo_results';
end

ids = readtable(fullfile(dataRootPath, 'politicians_id.csv'));
votesHistory = readtable(fullfile(dataRootPath, 'politicians_votes.csv'));

% K is the length of ids
K = size(ids, 1);
T = size(votesHistory, 1);

% single execution part
eta = 0.02;
tic
[lossHistory, trueLossHistory] = sequence_prediction.ewa_binary_sleeping(...
K, T, votesHistory, eta);
cumLossHistory = cumsum(lossHistory);
cumRealLossHistory = cumsum(trueLossHistory);
averageCumLossHistory = cumLossHistory ./ (1:T).';
averageRealLossHistory = cumRealLossHistory ./ (1:T).';
elapsedTime = toc

%{
 figure
plot(1:T, averageCumLossHistory);
xlabel('$t$', 'Interpreter', 'latex')
ylabel('$(1/t)\hat{L}_t$', 'Interpreter', 'latex')
title('Cumulated Expected Loss', 'Interpreter','latex') 
%}

% saveas(gcf, fullfile(outputPath, 'average_cum_loss_history_ogd_small.eps'), 'eps2c');

figure
hold on;
plot(1:T, averageRealLossHistory);
plot(1:T, 0.5 * ones(size(averageRealLossHistory)))
xlabel('$t$', 'Interpreter', 'latex')
ylabel('$(1/t)\sum_{s=1}^t |\hat{Y}_s - y_s|$', 'Interpreter', 'latex')
title('Average Loss', 'Interpreter','latex')


%{

% run multiple executions
etas = linspace(0., .5, 100).';
cumLossHistories = zeros(size(etas));
for k = 1:size(etas, 1)
    eta = etas(k, :);
    tic;
    lossHistory = sequence_prediction.ogd_binary_sleeping(...
        K, T, votesHistory, eta);
    cumLossHistory = cumsum(lossHistory);
    elapsedTime = toc;
    fprintf('Round [%d/%d], Compute for eta = %f , Time Cost : %.2f .\n ', ...
        k, size(etas, 1), eta, elapsedTime);
    averageCumLossHistory = cumLossHistory ./ (1:T).';
    cumLossHistories(k, 1) = cumLossHistory(T, 1);
end

cumLossResult = [etas cumLossHistories];
save(fullfile(outputPath, 'cum_loss_ogd_big_range_politicians.mat'), 'cumLossResult');

savedFile = matfile(fullfile(outputPath, 'cum_loss_ogd_big_range_politicians.mat'));
cumLossResult = savedFile.cumLossResult;

figure;
hold on
plot(cumLossResult(:, 1), cumLossResult(:, 2));
xlabel('\eta')
ylabel('$\sum_{t=1}^T l(\hat{y}_t, y_t)$', 'Interpreter', 'latex')
title('Cumulated Loss at time $T$', 'Interpreter','latex')
% saveas(gcf, fullfile(outputPath, 'average_cum_loss_history_ogd_small.eps'), 'eps2c');

%}
