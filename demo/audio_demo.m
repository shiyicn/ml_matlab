% Implement Gaussian Source Separation Method
if ispc
    datapath = 'C:\\Users\\Huang Zuli\\proj\\data\\audio\\waspaa';
else
    datapath = '~/proj/data/audio/waspaa';
end

% Add the library package path
if ispc
    addpath('C:\\Users\\Huang Zuli\\proj\\ml_matlab')
else
    addpath('~/proj/ml_matlab/')
end

m = 4;
n = 4;

% audioSequencesName = ['waspaa1_noisefree_x1.wav'; ...
%                       'waspaa1_noisefree_x2.wav'; ...
%                       'waspaa1_noisefree_x3.wav'; ...
%                       'waspaa1_noisefree_x4.wav'];

audioSequencesName = ['waspaa1_noisy_x1.wav'; ...
                      'waspaa1_noisy_x2.wav'; ...
                      'waspaa1_noisy_x3.wav'; ...
                      'waspaa1_noisy_x4.wav']; 

winname = 'sine';

% Read audio file
lFrame = 512;
nFrame = 128;
nBlock = 32;

N = lFrame * nFrame;

blockSize = lFrame / nBlock;

Xtilde = zeros(m, lFrame, nFrame); 
Xinit = zeros(N, m);

for asn = 1:m
    [X, FS] = audioread(fullfile(datapath, audioSequencesName(asn, :)));
    Xinit(:, asn) = X;
    xtilde = signalProcessing.mdctWindowed(X, lFrame, 'winname', winname);
    Xtilde(asn, :, :) = xtilde;
end
fprintf('l_frame : %d, and n_frame : %d.\n', lFrame, nFrame)

A0 = rand(m, n);
% A0 = [1 1 1 1; 0.8 1.3 -0.9 1; 1.2 -0.7 1.1 0.6; 0.6 -0.8 0.5 1.2];
J0 = diag(rand(m, 1));
S0 = rand(nBlock, nFrame, n);

[sourcesTilde, A, convergenceOfA, history] = signalProcessing.maxLHSourceSeparation( ...
                                    Xtilde, nBlock, A0, J0, S0, n, ...
                                    'maxIter', 100, 'epsilon', 0.0001, ...
                                    'getHistory', true);

% Use the inverse transform to recover the signals
sources = zeros(N, n);
% start to recover the signal
for sid = 1:n
    tic
    sources(:, sid) = signalProcessing.imdctWindowed(...
        squeeze(sourcesTilde(:, :, sid)), lFrame, 'winname', winname);
    elapsedTime = toc;
    % save separated sources into wave file
    filename = sprintf('s_est_%d.wav', sid);
    audio2save = sources(:, sid) / max(abs(sources(:, sid)));
    audiowrite(fullfile(datapath, 'results', filename), audio2save, FS);
    fprintf('Reconstruct source [%d/%d], time cost : %f s.\n', sid, n, ...
            elapsedTime)
end

% plot the convergence of the matrix A
figure;
semilogy(convergenceOfA);
xlabel('Iteration', 'Interpreter', 'latex');
ylabel('$\|\textbf{A}_t - \textbf{A}_{t-1}\|_\infty$', 'Interpreter', 'latex');
title('Convergence of $\textbf{A}$', 'Interpreter','latex');


% =============================================================================
% start to evaluate the performance of the algorithm
if ispc
    addpath('C:\\Users\\Huang Zuli\\scoop\\lib_source\\signals\\bss_eval');
else
    addpath('~/proj/ml_matlab/external_sources/bss_eval')
end

oracleSequencesName = ['waspaa1_det_oracle_se1.wav'; ...
                      'waspaa1_det_oracle_se2.wav'; ...
                      'waspaa1_det_oracle_se3.wav'; ...
                      'waspaa1_det_oracle_se4.wav']; 

% load oracle audios 
oracles = zeros(N, n);
for asn = 1:n
    [X, FS] = audioread(fullfile(datapath, oracleSequencesName(asn, :)));
    oracles(:, asn) = X;
end

originalSourcesName = ['waspaa1_s1.wav'; ...
                        'waspaa1_s2.wav'; ...
                        'waspaa1_s3.wav'; ...
                        'waspaa1_s4.wav'];

% load original sources 
originals = zeros(N, n);
for asn = 1:n
    [X, FS] = audioread(fullfile(datapath, originalSourcesName(asn, :)));
    originals(:, asn) = X;
end

sources_estimated = ['s_est_1.wav'; ...
                        's_est_2.wav'; ...
                        's_est_3.wav'; ...
                        's_est_4.wav'];

% load original sources 
sources = zeros(N, n);
for asn = 1:n
    [X, FS] = audioread(fullfile(datapath, 'results', sources_estimated(asn, :)));
    sources(:, asn) = X;
end

permSources = [2 4 1 3];

% ===========================================================================
% Evaluate the history

sourcesEstimated = zeros(N, n);
sdrs = zeros(size(history, 1), n);
sirs = zeros(size(history, 1), n);
sars = zeros(size(history, 1), n);
for estId = 1:size(history, 1)
    % start to recover the signal
    tic
    for sid = 1:n
        sourcesEstimated(:, sid) = signalProcessing.imdctWindowed(...
            squeeze(history(estId, :, :, sid)), lFrame, 'winname', winname);
    end
    [SDR, SIR, SAR, perm] = bss_eval_sources(sourcesEstimated', originals');
    sdrs(estId, :) = SDR;
    sirs(estId, :) = SIR;
    sars(estId, :) = SAR;
    elapsedTime = toc;
    fprintf('Iteration [%d/%d], time cost %f s.\n', estId, size(history, 1), ...
            elapsedTime);
end

figure
plot(sdrs)
xlabel('Iteration', 'Interpreter', 'latex')
ylabel('SDR', 'Interpreter', 'latex')
