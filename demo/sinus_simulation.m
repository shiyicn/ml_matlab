N = 1000;

X = linspace(0, 5, N)';
y = sin(X);
y_noised = y + normrnd(0, 0.2, size(y));

% partition the data into 2 part
perm = randperm(N);
splitRatio = 0.8;
split = floor(N * splitRatio);
permTrain = perm(1:split);
permVal = perm((split+1):N);
X_train = X(permTrain, :);
y_train = y_noised(permTrain, :);
X_val = X(permVal, :);
y_val = y_noised(permVal, :);
