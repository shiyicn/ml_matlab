addpath('~/proj/ml_matlab/')

rng(1); % For reproducibility

N = 1000;

r = sqrt(rand(N, 1)); % Radius
t = 2 * pi * rand(N, 1);  % Angle
data1 = [r .* cos(t), r .* sin(t)]; % Points

r2 = sqrt(3 * rand(N, 1) + 1); % Radius
t2 = 2 * pi * rand(N, 1);      % Angle
data2 = [r2 .* cos(t2), r2 .* sin(t2)]; % points

X_train = [data1; data2];
y_train = [ones(N, 1) ; -zeros(N, 1)];

% gaussian kernel logistic regression
sigma = .1;
pen = 0.01;
K_train = kernel.kers.gaussian_kernel(X_train, sigma);
a = kernel.logistic.logistic_regression(K_train, y_train, pen);
y_predicted_train = K_train * a;
y_predicted_train = int8(y_predicted_train >= 0);

% Predict scores over the grid
d = 0.02;
[x1Grid, x2Grid] = meshgrid(min(X_train(:,1)):d:max(X_train(:,1)), ...
                            min(X_train(:, 2)):d:max(X_train(:,2)));
xGrid = [x1Grid(:), x2Grid(:)];
scores = kernel.kers.gaussian_kernel(X_train, sigma, 'new_input', xGrid) * a;

% Plot the data and the decision boundary
figure;
h(1:2) = gscatter(X_train(:,1), X_train(:,2), y_train, 'rb', '.');
hold on
ezpolar(@(x)1);
contour(x1Grid, x2Grid, reshape(scores, size(x1Grid)), [0 0], 'k');
axis equal
hold off
