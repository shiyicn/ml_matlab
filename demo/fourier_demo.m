% Add the library package path
if ispc
    addpath('C:\\Users\\Huang Zuli\\proj\\ml_matlab')
else
    addpath('~/proj/ml_matlab/')
end

% --------------------------------------------------------------------------- %
% Random Shuffling of the transform of a Dirac
% --------------------------------------------------------------------------- %
N = 1001;
x = zeros(N, 1);
x(round(N / 2), :) = 1;

xHat = fft(x);
xInverse = ifft(xHat);

xHatShuffled = xHat(randperm(N), :);
xShuffledInversed = ifft(xHatShuffled);

figure;
histogram(abs(real(xShuffledInversed)) ./ abs(xShuffledInversed));

figure;
plot(abs(xHat).^2);
% --------------------------------------------------------------------------- % 

% --------------------------------------------------------------------------- % 
% Switch The Phase of two images
% --------------------------------------------------------------------------- % 
img1 = imread('cameraman.tif');
img2 = imread('pout.tif');


% --------------------------------------------------------------------------- % 

% --------------------------------------------------------------------------- %
% Given random process X(t) = A0 cos(w0*t + phi), phi is uniform in [-pi, pi]
N = 1000;
A0 = 2;
w0 = 0;
phi = rand([N, 1]);
t = (1:N)';
X = A0 * exp(w0 * t + phi * j);
XHat = ifft(X);

figure;
plot(fftshift(abs(XHat).^2));
% --------------------------------------------------------------------------- %

% --------------------------------------------------------------------------- %
% Power Spectrum of a Stochastic Process, Gaussian Noise
% --------------------------------------------------------------------------- %
NRealisations = 3;
RAveraged = 0;
XTransModulus = 0;
for iter = 1:NRealisations
    N = 1000;
    X = normrnd(0, 1, [N, 1]);

    XHat = fft(X);

    muHat = mean(X(:));
    R = xcorr(X) / N;

    RAveraged = RAveraged + R;
    XTransModulus = XTransModulus + abs(fft(XHat)).^2;
end

RAveraged = RAveraged / NRealisations;
RTransAveraged = fft(RAveraged);
XTransModulus = XTransModulus / NRealisations;
plot(abs(RTransAveraged) .^ 2);


% Add ScatNet package
addpath('~/proj/ml_matlab/external_sources/scatnet/');
addpath_scatnet;

% --------------------------------------------------------------------------- %
scat = signalProcessing.Scattering();

% --------------------------------------------------------------------------- %
% fractional brownian motion

% Quite Efficient As Hurst Exponent Measurement, Good Indicator
NSimulations = 1000;
pxxfBnAvg = 0;
mrafBnVariancesAvg = 0;
mrafBnSparsityAvg = 0;
for i = 1:NSimulations
    N = 10000;
    % Generate fBm for H = 0.3 and H = 0.7
    H = 0.55; 
    % Generate and plot wavelet-based fBm for H = 0.3
    fBm = wfbm(H, N)';
    fBn = diff(fBm);
    lev = 7;
    mrafBnVariances = scat.moments(1, fBn, lev, 'db2', 2);
    mrafBnSparsity = scat.sparsity(fBn, lev, 'db2'); % sparsity calculation problem
    pxxfBnAvg = pxxfBnAvg + (abs(fft(fBn)).^2) / (2 * N);
    mrafBnVariancesAvg = mrafBnVariances + mrafBnVariancesAvg;
    mrafBnSparsityAvg = mrafBnSparsityAvg + mrafBnSparsity;
end
pxxfBnAvg = pxxfBnAvg / NSimulations;
mrafBnSparsityAvg = mrafBnSparsityAvg / NSimulations;
mrafBnVariancesAvg = mrafBnVariancesAvg / NSimulations;
semilogy(fftshift(pxxfBnAvg));
plot(log2(mrafBnVariancesAvg));
plot(mrafBnSparsityAvg);

% --------------------------------------------------------------------------- %

% --------------------------------------------------------------------------- %
% White Gaussian Noise
N = 10000;
X = normrnd(0, 1, [N, 1]);
lev = 10;

% compute the sparsity
mraGNVariances = scat.moments(1, X, lev, 'db2', 2);
mraGNSparsity = scat.sparsity(X, lev, 'db2');
plot(log2(mraGNVariances));
plot(mraGNSparsity);

% --------------------------------------------------------------------------- %

% --------------------------------------------------------------------------- %
% Poisson Process
% --------------------------------------------------------------------------- %

NSimulations = 20;
mraDiracCombPoissonVariancesAvg = 0;
for iter = 1:NSimulations
    N = 5000;
    X = simulation.poissonProcess(N, 0.03);
    lev = 10;
    diracCombPossion = diff(X);
    pxxDiracCombPoisson = abs(fft(diracCombPossion)).^2;
    mraDiracCombPoissonVariancesAvg = mraDiracCombPoissonVariancesAvg + ...
                                        scat.moments(1, diracCombPossion, ...
                                                     lev, 'db2', 2);
end

mraDiracCombPoissonVariancesAvg = mraDiracCombPoissonVariancesAvg / NSimulations;

plot(log2(mraDiracCombPoissonVariancesAvg));

% --------------------------------------------------------------------------- %
