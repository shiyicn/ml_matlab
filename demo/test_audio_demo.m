% Implement Gaussian Source Separation Method
if ispc
    datapath = 'C:\\Users\\Huang Zuli\\proj\\data\\audio\\waspaa';
else
    datapath = '~/proj/data/audio/waspaa';
end

% Add the library package path
if ispc
    addpath('C:\\Users\\Huang Zuli\\proj\\ml_matlab')
else
    addpath('~/proj/ml_matlab/')
end

m = 4;
n = 4;


audioSequencesName = ['waspaa1_noisefree_x1.wav'; ...
                      'waspaa1_noisefree_x2.wav'; ...
                      'waspaa1_noisefree_x3.wav'; ...
                      'waspaa1_noisefree_x4.wav'];    

% audioSequencesName = ['waspaa1_noisy_x1.wav'; ...
%                       'waspaa1_noisy_x2.wav'; ...
%                       'waspaa1_noisy_x3.wav'; ...
%                       'waspaa1_noisy_x4.wav'];

% Read audio file
lFrame = 512;
nFrame = 128;
nBlock = 32;

alphaRate = 8;

N = lFrame * nFrame;

blocSize = lFrame / nBlock;

Xtilde = zeros(m, lFrame, nFrame); 
Xinit = zeros(N, m);

for asn = 1:m
    [X, FS] = audioread(fullfile(datapath, audioSequencesName(asn, :)));
    Xinit(:, asn) = X;
    xtilde = signalProcessing.mdctSineBell(X, lFrame);
    Xtilde(asn, :, :) = xtilde;
end
fprintf('l_frame : %d, and n_frame : %d.\n', lFrame, nFrame)

g = wilwin('sine', lFrame);
x = Xinit(:, 1);
xtilde = wmdct(x, g, lFrame);
xinv = iwmdct(xtilde, g);

figure
plot(g)
xlabel('$n$', 'Interpreter', 'latex')
ylabel('$w[n]$', 'Interpreter', 'latex')
title('Window Function $w$', 'Interpreter','latex')
