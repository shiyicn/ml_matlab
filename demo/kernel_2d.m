addpath('~/proj/ml_matlab/kernel/kers')
addpath('~/proj/ml_matlab/kernel/ridge')
addpath('~/proj/ml_matlab/metrics')

N = 100; % num of data points

[X1, X2] = meshgrid(linspace(0, 5, N), linspace(0, 5, N));
X = [X1(:), X2(:)]; % construct multi-dimension array
w = [1; 1];
y = sin(X * w);
y_noised = y + normrnd(0, 0.2, size(y));

N_data = N * N;

% partition the data into 2 part
perm = randperm(N_data);
splitRatio = 0.8;
split = floor(N_data * splitRatio);
permTrain = perm(1:split);
permVal = perm((split+1):N_data);
X_train = X(permTrain, :);
y_train = y_noised(permTrain, :);
X_val = X(permVal, :);
y_val = y_noised(permVal, :);

sigma = 1.;
pen = 0.01;
K_train = gaussian_kernel(X_train, sigma);
a = ridge_regression(K_train, y_train, pen);
y_predicted_train = K_train * a;

K_val = gaussian_kernel(X_train, sigma, 'new_input', X_val);
y_predicted_val = K_val * a;

errors_train = rms(y_predicted_train, y(permTrain, :));
errors_val = rms(y_predicted_val, y(permVal, :));

fprintf('Train RMS Error: %.4f.\n', errors_train);
fprintf('Validation RMS Error: %.4f.\n', errors_val);
