function A = readcsv(file_path)
    % readcsv - Description
    %
    % Syntax: A = readcsv(file_path)
    %
    % Long description
    blocksize = 5000; 
    maxrows = 2500000; 
    cols = 20; 
    rp = 1; % row pointer
    % Preallocate A to its maximum possible size 
    A = zeros(maxrows, cols);

    % Open the data file, saving the file pointer. 
    fid = fopen(file_path, 'r');

    while true
        % Read from file into a cell array. Stop at EOF.
        block = textscan(fid, '%n', blocksize*cols);
        if isempty(block{1})
            break;
        end
        % Convert cell array to matrix, reshape, place into A.
        A(rp:rp+blocksize-1, 1:cols) = ...
            reshape(cell2mat(block), blocksize, cols);
        % Process the data in A.
        evaluate_stats(A);  % User-defined function
        % Update row pointer 
        rp = rp + blocksize;
    end
end