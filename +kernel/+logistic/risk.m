%{
required functions : sigmoid in 'basic' package
addpath('~/proj/ml_matlab/basic')
%}

function r = risk(K, a, y, pen)
    %{
    ================================================================
    Function: 
    ---------
    argmin R(a) = 1/n * l(<K, a> - y) + pen * || \sum_{i=1}^n a_i * K(x_i, .) ||
    
    where the loss function is 
    l(y', y) = -y * log(s(y')) - (1-y)log(1-s(y')), the negative log-likelihood
    thus, the problem can be re-written as 
    argmin 1/n * \sum_{i=1}^n y_i * log(K*a _i) + (1-y_i) * log((1-K*a _i)) + pen * a^T * K * a

    Args: 
    -----
    K : matrix-like SPD kernel from the X matrix
    y : the ground truth label
    pen : the penalisation constant

    Example:
    --------
    >> X = rand(100, 30);
    >> y = rand(100)
    >> K = gaussian_kernel(X, 2);
    >> a = logistic_regression(K, y, 0.1);
    
    Returns:
    --------
    a : numerical vector 
    ================================================================
    %}
    K_sigma = sigmoid(K * a);
    n = size(K, 1);
    r = -1/n * (dot(log(K_sigma), y) + dot(log(1 - K_sigma), 1-y)) ...
        + pen * a' * K * a;
end
