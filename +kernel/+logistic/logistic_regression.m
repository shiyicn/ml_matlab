%{
addpath('~/proj/ml_matlab/basic/')
%}

function a = logistic_regression(K, y, pen, varargin)
    %{
    ================================================================
    Function: 
    ---------
    argmin R(a) = 1/n * l(f(x_i), y_i) + pen * ||f||
    with f = a * K(x, .)
    
    where the loss function is 
    l(y', y) = -y * log(s(y')) - (1-y)log(1-s(y')), the negative log-likelihood
    
    using newton's method, solve the equation R'(a) = 0
    R'(a) = 0, start from an initial point a0
    a_{n+1} = a_n - H(R(a_n))^{-1} * R'(a)

    Args: 
    -----
    K : matrix-like SPD kernel from the X matrix
    y : the ground truth label
        label should be encoded in {0, 1}
    pen : the penalisation constant
    epsilon : control the perturbation level for the stability of the convergence when Hessian matrix is not invertible

    Example:
    --------
    >> X = rand(100, 30);
    >> y = rand(100, 1);
    >> K = gaussian_kernel(X, 2); % sigma is set to be 2
    >> pen = 1.; % penalisation factor
    
    Returns:
    --------
    a : numerical vector 

    ================================================================
    %}
    n = size(K, 1);
    max_iter = 10; % max iterations of newton's method iterations episodes
    a = zeros(n, 1); % initial value should be carefully set

    epsilon = 1e-6; % add perturbation to hessian matrix

    for iter = 1:max_iter
        % use newton's method to solve the optimization problem
        K_sigma = basic.sigmoid(K * a); % compute the kernel sigmoid value
        grad_R = -1/n * K * (y - K_sigma) + ...
                 + 2 * pen * K * a; % compute the gradient of R(a)
        hessian_R = 1/n * K * (K_sigma .* (1-K_sigma) .* K') + 2 * pen * K;
        % check if H(a) is invertible, if not a perturbation should be added.
        if rcond(hessian_R) < 1e-10
            hessian_R = hessian_R + epsilon * eye(n);
        end
        a = a - hessian_R \ grad_R; % newton method iteration
    end
end
