function K = gaussian_kernel(X, sigma, varargin)
    %{
    ================================================================
    Function: 
    ---------
    Compute the SPD Gaussian Kernel given a set of input vectors
    Given an input feature vectors encoded in the matrix X, we have that 
    X_{i .} denotes the i-th feature vector. The kernel K is thus
    K_{ij} = exp{ - ||x_i - x_j||^2 / (2 * sigma^2) }

    Args: 
    -----
    X : input feature matrix, X_{i .} denotes the i-th feature vector
    sigma : positive real numerical value
    X_new : after the flat 'new_input'
            optional, by default it would be the input matrix X itself

    Example:
    --------
    >> X = rand(2, 3);
    >> K = gaussian_kernel(X, 1);
    
    Returns:
    --------
    K : the gaussian kernel SPD matrix
    ================================================================
    %}
    temp = find(strcmp(varargin, 'new_input') == 1); % search for frequency
    if isempty(temp)                            % if not given
        X_new = X;                              % default is the X itself
    else                                        % if user supplies X_new
        X_new = varargin{temp+1};                % assign to user input
        varargin(temp:temp+1) = [];             % remove from varargin
    end
    clear temp
    % decompose the kernel calculation
    % ||x_i - x_j||_2^2 = ||x_i||_2^2 + ||x_j||_2^2 - 2<x_i, x_j>
    
    pattern_X = sum(X .* X, 2);
    pattern_X_new = sum(X_new .* X_new, 2);
    K = exp(-(pattern_X' + pattern_X_new - 2 * X_new * X') / (2 * sigma^2));
end
