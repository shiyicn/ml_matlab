function v = mixedSobolevKer(x, y)
    % Mixed Sobolev Kernel
    %
    % Syntax: v = mixedSobolevKer(x, y)
    %
    % Long description

    if 0 <= min(x, y) && 1 >= max(x, y)
        a = x - 1.;
        b = -x / sin(1.) + a * cot(1.);
        if y <= x
            v = (1 - x) + a * cos(y) - b * sin(y);
        else
            v = -x + a * cos(y) - b * sin(y);
        end
    else
        fprintf('Invalid input x : %.3f and y : %.3f', x, y)
    end
end
