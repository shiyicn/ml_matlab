function y = predict(X, a, varargin)
    n = size(K, a);
    temp = find(strcmp(varargin, 'isSuppVec') == 1); % search for method name
    if isempty(temp)                            % if not given
        isSuppVec = logical(ones());                   % default is the X itself
    else                                        % if user supplies X_new
        thresholdSupp = varargin{temp+1};                % assign to user input
        varargin(temp:temp+1) = [];             % remove from varargin
    end

    y = sign(K * a);
end
