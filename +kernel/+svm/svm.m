%{
addpath('~/proj/ml_matlab/basic')
%}

function [a, isSuppVec] = svm(K, y, pen, varargin)
    %{
    ================================================================
    Function: 
    ---------
    Implement the Support Vector Machine for classification problem
    Primal : 
        minimize 1/n * \sum_{i=1}^n eta_i + pen * a' * K * a
        s.t.     eta_i >= 0 and y_i * (Ka)_i + eta_i - 1>= 0
    Dual :
        maximize \sum_{i=1}^n mu_i - 1/(4*pen) * \sum_{i, j} y_i * y_j * mu_i * mu_j * K(x_i, x_j)

    Args: 
    -----
    K : matrix-like SPD kernel from the X matrix
    y : the ground truth label
    pen : the penalisation constant
    algo : 'lambda' and '2-svm' two type of SVM algorithm
    thresholdSupp : threshold for support vectors

    Example:
    --------
    >> X = rand(100, 30);
    >> y = randi([0, 1], 100, 1);
    >> K = gaussian_kernel(X, 2); % sigma is set to be 2
    >> pen = 1.; % penalisation factor
    >>
    
    Returns:
    --------
    a : numerical vector 
    ================================================================
    %}

    % Use Quadratic Programming method to solve the SVM problem
    % quadprog
    n = size(K, 1);

    temp = find(strcmp(varargin, 'method') == 1); % search for method name
    if isempty(temp)                            % if not given
        algo = 'lambda';                              % default is the X itself
    else                                        % if user supplies X_new
        algo = varargin{temp+1};                % assign to user input
        varargin(temp:temp+1) = [];             % remove from varargin
    end

    temp = find(strcmp(varargin, 'threshold') == 1); % search for method name
    if isempty(temp)                            % if not given
        thresholdSupp = 5e-3;                              % default is the X itself
    else                                        % if user supplies X_new
        thresholdSupp = varargin{temp+1};                % assign to user input
        varargin(temp:temp+1) = [];             % remove from varargin
    end
    
    clear temp
    
    if strcmp(algo, 'lambda')
        H = 1 / (2 * pen) * diag(y) * K * diag(y);
        f = -ones(n, 1);
        lb = zeros(n, 1);
        ub = 1 / n * ones(n, 1);
        [mu, fval, exitflag, output, lambda] = ...
                quadprog(H, f, [], [], [], [], lb, ub);
        a = diag(y) * mu / (2 * pen);
    elseif strcmp(algo, '2-svm')
        H = 2 * (K + n * pen * eye(n));
        f = -2 * y;
        A = -diag(y);
        b = zeros(size(y));
        [a, fval, exitflag, output, lambda] = ...
                quadprog(H, f, A, b);
    else
        fprintf('Method %s is not supported!\n', algo);
    end

    % retrieve the support vectors
    isSuppVec = (abs(a) >= thresholdSupp);
end
