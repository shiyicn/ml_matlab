function a = ridge_regression(K, y, pen)
    %{
    ================================================================
    Function: 
    ---------
    argmin 1/n * || <K, a> - y || + pen * || \sum_{i=1}^n a_i * K(x_i, .) ||

    Args: 
    -----
    K : matrix-like SPD kernel from the X matrix
    y : the ground truth label
    pen : the penalisation constant

    Example:
    --------
    >> X = rand(100, 30);
    >> y = rand(100)
    >> K = gaussian_kernel(X, 2);
    >> a = ridge_regression(K, y, 0.1);
    
    Returns:
    --------
    a : numerical vector 
    ================================================================
    %}
    n = size(K, 1);
    a = (K + pen * n * eye(n)) \ y;
end
