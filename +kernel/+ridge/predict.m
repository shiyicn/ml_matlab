function y_new = predict(K_new, a)
    %{
    ================================================================
    Function: 
    ---------
    a \in argmin 1/n * || <K, a> - y || + pen * || \sum_{i=1}^n a_i * K(x_i, .) ||
    use X, X_new, a and the kernel function to predict the y_new

    Args: 
    -----
    K_new : the kernel matrix computed by X and X_new
    a : vector-like coefficients

    Example:
    --------
    
    Returns:
    --------
    y_new : numerical vector 
    ================================================================
    %}
    y_new = K_new * a;
end
