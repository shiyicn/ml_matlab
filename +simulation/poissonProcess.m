function [Ts, arrivals] = poissonProcess(Tmax, lambda)
	clear T;
	Ts = random('Exponential', 1/lambda, [Tmax, 1]);
	X = cumsum([0; Ts]);
	[Ts, arrivals] = stairs(X, 0:Tmax);
end
