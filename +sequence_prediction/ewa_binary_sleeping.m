function [lossHistory, trueLossHistory] = ewa_binary_sleeping(K, T, votesHistory, eta)

    % import projection method
    import sequence_prediction.lossBernouilli

    cumTildeLoss = zeros(2 * K, 1);
    activeActions = [1; 0; 0; 1];
    lossHistory = zeros(2 * K, 1);
    trueLossHistory = zeros(2 * K, 1);

    for t = 1:T
        % Environment Setting Up
        z1 = votesHistory{t, 2};
        z2 = votesHistory{t, 3};
        activeActionsRange = [z1, z2, K + z1, K + z2];

        % Agent makes its decision
        %{
        pTilde = exp(-eta * (cumTildeLoss - min(cumTildeLoss)));
        pt = pTilde(activeActionsRange, :);
        pt = pt / sum(pt);
        %}
        pt = -eta * cumTildeLoss(activeActionsRange, :);
        pt = exp(pt - min(pt));
        pt = pt / sum(pt);
        % select the decision by \tilde{p}
        ytHat = dot(activeActions, pt);
        ytChoice = int32(rand() <= ytHat);
        yt = votesHistory{t, 4}; % winner can be either z1 or z2, 1 and 0 resp.

        instantLoss = lossBernouilli(ytHat, yt);
        realInstantLoss = abs(ytChoice - yt);
        lossHistory(t, :) = instantLoss;
        trueLossHistory(t, :) = realInstantLoss;
        % update the loss info
        currentActionTmp = cumTildeLoss(activeActionsRange);
        cumTildeLoss = cumTildeLoss + instantLoss;
        % correct the active actions
        activeLoss = lossBernouilli(activeActions, yt);
        cumTildeLoss(activeActionsRange, :) = currentActionTmp + activeLoss;
    end
end
