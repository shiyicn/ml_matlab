function [lossHistory, trueLossHistory] = ogd_binary_sleeping(K, T, ...
    votesHistory, eta)

    % import projection method
    import optim.proximal.simplex_projection
    import sequence_prediction.lossBernouilli

    activeActions = [1; 0; 0; 1];
    lossHistory = zeros(2 * K, 1);
    trueLossHistory = zeros(2 * K, 1);
    
    % initialize with an arbitrary action
    pTilde = ones(2 * K, 1) / K;
    
    for t = 1:T
        % Environment Setting Up
        z1 = votesHistory{t, 2};
        z2 = votesHistory{t, 3};
        activeActionsRange = [z1, z2, K + z1, K + z2];

        % Agent makes its decision
        if all(pTilde(activeActionsRange, :) == 0)
            pt = ones(4, 1) * 0.25; 
        else
            pt = pTilde(activeActionsRange, :);
            pt = pt ./ sum(pt);
        end
        % select the decision by \tilde{p}
        ytHat = dot(activeActions, pt);
        ytChoice = int32(rand() <= ytHat);
        yt = votesHistory{t, 4}; % winner can be either z1 or z2, 1 and 0 resp.

        instantLoss = lossBernouilli(ytHat, yt);
        realInstantLoss = abs(ytChoice - yt);
        lossHistory(t, :) = instantLoss;
        trueLossHistory(t, :) = realInstantLoss;

        % compute the gradient
        %{
        % first senario
        pTildeActive = pTilde(activeActionsRange, :);
        gradActive = (activeActions - dot(pTildeActive, activeActions)) ...
                     / sum(pTildeActive .^ 2) * (1 - 2 * yt);
        pTilde(activeActionsRange, :) = pTilde(activeActionsRange, :) - ...
                                         eta * gradActive;
        %}
        gradTilde = instantLoss * ones(2 * K, 1);
        gradTilde(activeActionsRange, :) = lossBernouilli(activeActions, yt);
        
        pTilde = pTilde - eta * gradTilde;
        pTilde = simplex_projection(pTilde);
    end

end
