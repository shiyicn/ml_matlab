% define a local loss function
function loss = lossBernouilli(YHat, yt)
    % yt : numerical value
    loss = (1. - YHat) * yt + YHat * (1 - yt);
end
