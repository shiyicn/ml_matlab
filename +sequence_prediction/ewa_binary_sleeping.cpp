#include "mex.h" // headers for matlab interface
#include <stdio.h>
#include <cmath>

#define A_IN prhs[0]

using namespace std;

void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[]) {
    double *A, *cumLosses;
    A = mxGetPr(A_IN);
    // Start the Exponential Weighted Algorithm routine
    /** 
     * Combine the sleeping trick and EWA algorithm
     * Given the input : 
     * eta : the temperature parameter
     * K : the num of the possible actions
     */
    // At each round receive possible actions z1, z2
    short z1 = 0;
    short z2 = 0;

    K = 0;

    // define cumulated losses
    mxArray *cumLossesArray =  mxCreateDoubleMatrix(K, 1, mxREAL);
    double *cumLosses = mxGetPr(cumLossesArray);
    // initialize the array with zeros
    mxSetData(ZERO, cumLossesArray);

    
}