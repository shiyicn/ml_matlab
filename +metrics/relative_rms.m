function err = relative_rms(y, y_true)
    n = size(y, 1);
    err = sqrt(1 / n * sum((y ./ y_true - 1).^2));
end
