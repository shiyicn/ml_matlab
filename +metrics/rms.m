function err = rms(y, y_true)
    n = size(y, 1);
    err = sqrt(1 / n * sum((y - y_true).^2));
end
