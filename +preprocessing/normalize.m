function [X_normalised, X_mean, X_std] = normalize(X, varargin)
    %{
    ================================================================
    Function: 
    ---------
    Normalize the input matrix along a certain dimension.

    Args: 
    -----
    X : matrix-like numerical matrix
    X_mean : optional
             after flag 'mean', the pre-fixed mean
    X_std : optional
             after flag 'std', the pre-fixed standard deviation

    Example:
    --------
    >> X = rand(100, 30);
    >> [X_norm, X_mean, X_val] = normalize(X)
    
    Returns:
    --------
    X_normalised : numerical matrix-like, normalized X
    X_mean : the mean of X along the input dimension
    X_std : the standard deviation of X along the input dimensions
    ================================================================
    %}

    temp = find(strcmp(varargin, 'dim') == 1); % search for dimension
    if isempty(temp)                            % if not given
        dim = 1;                              % default is the 1st dimension
    else                                        % if user supplies dimension
        dim = varargin{temp+1};                % assign to user input
        varargin(temp:temp+1) = [];             % remove from varargin
    end
    temp = find(strcmp(varargin, 'mean') == 1); % search for dimension
    if isempty(temp)
        X_mean = mean(X, dim);
    else
        X_mean = varargin{temp+1};                % assign to user input
        varargin(temp:temp+1) = [];             % remove from varargin
    end
    temp = find(strcmp(varargin, 'std') == 1); % search for dimension
    if isempty(temp)
        X_std = std(X, dim);
    else
        X_std = varargin{temp+1};                % assign to user input
        varargin(temp:temp+1) = [];             % remove from varargin
    end
    clear temp
    
    X_normalised = bsxfun(@minus, X, X_mean);
    X_normalised = bsxfun(@rdivide, X_normalised, X_std);
end
