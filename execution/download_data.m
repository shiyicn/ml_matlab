% Script to download asset time series from yahoo finance 
% data source

% Add the library package path
if ispc
    addpath('C:\\Users\\Huang Zuli\\proj\\ml_matlab');
    datapath = 'C:\\Users\\Huang Zuli\\proj\\data';
else
    addpath('~/proj/ml_matlab/');
    datapath = '~/proj/data/';
end

% path to save the downloaded data feeds
sector = 'ETF';
rootDataPath = 'C:\\Users\\Huang Zuli\\proj\\data\\trading';
path2SaveData = fullfile(rootDataPath, sector);
% tickers of the assets to be downloaded
tickers = {'EWA', 'EWC', 'GLD', 'USO'};
% the start of the date for the time series
startDate = '26042000';
% the end of the date for the time series
endDate = '09042019';

for tickerCell = tickers
    ticker = tickerCell{1};
    % download time series according their tickers
    dataLoaded = trading.hist_asset_data.hist_stock_data(startDate, ...
                     endDate, ticker);
    assets = trading.hist_asset_data.singleAsset2csv(dataLoaded);
    writetable(assets, fullfile(path2SaveData, char(sprintf("%s.csv", ticker))), ...
                'WriteVariableNames', true);
end
