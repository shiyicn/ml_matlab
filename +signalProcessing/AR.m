% The ml_matlab package is needed to fit the AR model

classdef AR < handle
    properties
        coef
        intercept
        sigma
    end

    methods
        function obj = AR()
            obj.coef = 0;
            obj.intercept = 0;
            obj.sigma = 1;
        end

        function X = simulation(obj, d, N, sigma, coefInit, interceptInit)
            coef = coefInit;
            intercept = interceptInit;
            dep = size(coef, 1);
            X = zeros(N, d);
            
            noises = normrnd(0, sigma, [N, d]);
            X(1:dep, :) = normrnd(0, 1, [dep, d]);
            
            for k = (dep+1):N
                X(k, :) = coef' * X(k-dep:k-1, :) + intercept + noises(k, :);
            end
        end

        function predict(obj)
        end

        function residuals = residual(obj, X, coef, intercept)
            % it only supports 1d convolution
            Xfiltered = conv(X, coef, 'valid');
            dep = size(coef, 1);
            validSize = size(Xfiltered, 1) - 1;
            residuals = X(dep+1:end, :) - Xfiltered(1:validSize, :);
        end

        function [coef, intercept] = fit(obj, X, dep, varargin)

            temp = find(strcmp(varargin,'fitIntercept') == 1); % search for if fit intercept
            if isempty(temp)                                   % if not given
                fitIntercept = false;                          % default is not fit the intercept
            else                                               % if user supplies type
                fitIntercept = varargin{temp+1};               % assign to user input
                varargin(temp:temp+1) = [];                    % remove from varargin
            end
            clear temp

            coef = 0;
        end
    end
end