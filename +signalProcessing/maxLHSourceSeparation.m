function [sourcesTilde, A, convergenceOfA, history] = maxLHSourceSeparation(Xtilde, ...
    nBlock, A0, J0, S0, n, varargin)
    
    % set up the WMDCT configuration
    [m, lFrame, nFrame] = size(Xtilde);

    N = lFrame * nFrame;
    
    if mod(lFrame, nBlock) ~= 0
        fprintf('The frequence l_frame : [%d] is not dividable by nblocks : [%d].\n', ...
                lFrame, nBlock);
    end
    
    blockSize = lFrame / nBlock;

    % determine if user specified max iteration
    temp = find(strcmp(varargin, 'maxIter') == 1); % search for max iteration
    if isempty(temp)                            % if not given
        maxIter = 30;                           % default is daily
    else                                        % if user supplies max iteration
        maxIter = varargin{temp+1};             % assign to user input
        varargin(temp:temp+1) = [];             % remove from varargin
    end
    clear temp

    % determine if user specified epsilon precision
    temp = find(strcmp(varargin, 'epsilon') == 1); % search for epsilon precison
    if isempty(temp)                               % if not given
        epsilon = -Inf;                            % default is minus infinity
    else                                           % if user supplies epsilon
        epsilon = varargin{temp+1};                % assign to user input
        varargin(temp:temp+1) = [];                % remove from varargin
    end
    clear temp

    % determine if user specified if the history should be logged
    temp = find(strcmp(varargin, 'getHistory') == 1); % search for log flag
    if isempty(temp)                                  % if not given
        getHistory = false;                           % default is false
    else                                              % if user supplies the flag
        getHistory = varargin{temp+1};                % assign to user input
        varargin(temp:temp+1) = [];                   % remove from varargin
    end
    clear temp
    
    % initialize the parameters
    A = A0;
    J = J0;
    S = S0;

    % configure the intermediate variables
    W = zeros(nBlock, nFrame, n, m);
    C = zeros(nBlock, nFrame, n, n);
    Pss = zeros(nBlock, nFrame, n, n);
    tau = zeros(lFrame, nFrame, n);

    % compute the covariance matrix for the mixture of the sources
    Pxx = zeros(m, m);
    for qIter = 1:lFrame
        for kIter = 1:nFrame
            tmpVec = squeeze(Xtilde(:, qIter, kIter));
            Pxx = Pxx + tmpVec * tmpVec';
        end
    end
    Pxx = Pxx / N; 

    % initialize the convergence of matrix A 
    convergenceOfA = zeros(maxIter, 1);

    if getHistory
        history = zeros(maxIter, lFrame, nFrame, n);
    else
        history = 0;
    end

    for it = 1:maxIter
        tic % start time counting
        invJ = inv(J);

        % reset the matrix Pss, Pxs to be zero
        Pss = zeros(n, n);
        Pxs = zeros(m, n);

        for bIter = 1:nBlock
            for kIter = 1:nFrame
                tmpS = squeeze(S(bIter, kIter, :));
                invTmpS = diag(1.0 ./ tmpS);
                C(bIter, kIter, :, :) = inv(A' * invJ * A + invTmpS);
                W(bIter, kIter, :, :) = squeeze(C(bIter, kIter, :, :)) * A' * invJ;
            end
        end
        
        % reset S matrix to be zero
        S = zeros(nBlock, nFrame, n);
        for qIter = 1:lFrame
            for kIter = 1:nFrame
                % find the block when we have all the same block size
                % same block size for every block in {1, ..., n_block}
                bIter = ceil(qIter / blockSize);
                tmpW = squeeze(W(bIter, kIter, :, :)); % tmpW = W(q, k)
                tmpX = squeeze(Xtilde(:, qIter, kIter)); % tmpX = Xtilde(q, k)
                tmpC = squeeze(C(bIter, kIter, :, :)); % tmpC = C(b, k)
                tau(qIter, kIter, :) = tmpW * tmpX; % tau(q, k) := W(b, k) * Xtild(q, k)
                tmpPss = tmpW * (tmpX * tmpX') * tmpW' + tmpC;
                Pss = Pss + tmpPss;
                Pxs = Pxs + tmpX * squeeze(tau(qIter, kIter, :))';
                S(bIter, kIter, :) = squeeze(S(bIter, kIter, :)) + ...
                                    diag(1 / blockSize * tmpPss);
            end
        end

        Pss = Pss / N;
        Pxs = Pxs / N;

        tmpA = A;
        A = Pxs / Pss; % compute A = P_{xs} * P_{ss}^{-1}

        convergenceOfA(it) = max(abs(A(:) - tmpA(:)));
        J = diag(Pxx - Pxs / Pss * Pxs');
        % noise covariance matrix should always be positive
        J = max(J, epsilon);
        J = diag(J);

        % diag covariance should always be positive
        S = max(S, epsilon);
        
        % Stop time counting
        elapsedTime = toc;

        fprintf('Iteration [%d/%d], time cost : %f s.\n', it, maxIter, elapsedTime);

        if getHistory
            history(it, :, :, :) = tau;
        end

    end

    % Use the inverse transform to recover the signals
    sourcesTilde = tau;

end
