classdef Scattering < handle
    properties 
        wname
    end

    methods
        function obj = Scattering()
            obj.wname = 'db2';
        end

        function coef = scattering(J, varargin)
            
            J = 1;
            coef = 0;
        end

        function mraMoment = moments(obj, orderScattering, X, lev, wname, q)
            if length(size(X)) ~= 2 || size(X, 2) ~= 1
                fprintf('Invalid dimension of input X : [%s] .\n', size(X));
            end
            if orderScattering ~= 1
                frpintf('Invalid scattering moment order : [%d].\n', orderScattering);
            end
            % compute the sparsity
            [mraX, rangeX] = wavedec(X, lev, wname);
            detailIntervals = cumsum(rangeX); % compute the index of wavelet coefs
            mraMoment = zeros([lev, 1]);
            for kk = 1:lev
                rangeDetails = [detailIntervals(kk)+1:detailIntervals(kk+1)];
                mraMoment(kk, :) = mean(abs(mraX(rangeDetails, :)).^q);
            end
            mraMoment = flip(mraMoment);
        end

        function sparseCoef = sparsity(obj, X, lev, wname)
            if length(size(X)) ~= 2 || size(X, 2) ~= 1
                fprintf('Invalid dimension of input X : [%s] .\n', size(X));
            end
            % compute the sparsity
            [mraX, rangeX] = wavedec(X, lev, wname);
            detailIntervals = cumsum(rangeX); % compute the index of wavelet coefs
            sparseCoef = zeros([lev, 1]);
            for kk = 1:lev
                rangeDetails = [detailIntervals(kk)+1:detailIntervals(kk+1)];
                sparseCoef(kk, :) = mean(abs(mraX(rangeDetails, :)).^2) / ...
                                    mean(abs(mraX(rangeDetails, :))).^2;
            end
            sparseCoef = pi / 4 * flip(sparseCoef);
        end
    end

end
