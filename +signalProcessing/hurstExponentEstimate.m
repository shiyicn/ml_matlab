function H = hurstExponentEstimate(X, lev, varargin)
    %{
    % apply discrete wavelet transform to obtain Multiresolution Result
      
    
    %}
    % ---------------------------------------------------------------------------- %
    % todo : clean the code and find a good way to estimate hurst exponent

    temp = find(strcmp(varargin, 'type') == 1); % search for type
    if isempty(temp)                            % if not given
        event = 'history';                      % default is historical prices
    else                                        % if user supplies type
        event = varargin{temp+1};               % assign to user input
        varargin(temp:temp+1) = [];             % remove from varargin
    end
    clear temp

    % use a Multiresolution Analysis Method to decompose the series
    % start to recover the empirical variance informations
    mrafBmVariances = zeros(lev, 1); 
    detailIntervals = cumsum(l); % compute the index of wavelet coefs
    for kk = 1:lev
        rangeDetails = [detailIntervals(kk)+1:detailIntervals(kk+1)];
        mrafBmVariances(kk, :) = mean(mrafBm(:, rangeDetails).^2);
    end
    % inverse the order
    mrafBmVariances = flip(mrafBmVariances);
    plot(log2(mrafBmVariances)); % we can clearly see that the log-var is linear to the scale j

    % visualization of the wavelet coefs
    figure;
    subplot(lev+1, 1, 1)
    plot(fBm); % plot original series
    for kk = 1:lev
        subplot(lev+1, 1, kk+1)
        rangeDetails = [detailIntervals(kk):detailIntervals(kk+1)];
        plot(mrafBm(:, rangeDetails));
    end

    % process a linear regression method for the log MRA scales

    % -------------------------------------------------------------------------- %
    
end

