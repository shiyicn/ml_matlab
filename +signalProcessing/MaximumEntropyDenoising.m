% Multiple Useful functions for linear models

% Load linear algebra package
% addpath('linear_algebra')

classdef MaximumEntropyDenoising < handle

	properties
        y
        mu
    end

    methods

        function obj = MaximumEntropyDenoising(K, y)
            obj.y = y;
            obj.K = K;
        end

        function v = forwardBackward(K, y, inGamma, x0)
            maxIter = 10;
            x = x0;
            for l = 1:maxIter
                x = x - inGamma *(K.' * (K * x - y)); 
                x = optim.proximal.entProx(x, inGamma);
            end
            v = x;
        end
    end
    
end
