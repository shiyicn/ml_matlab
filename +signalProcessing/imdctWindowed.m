% To use the toolbox, 'cd' to the LTFAT directory and execute 'ltfatstart'
% Using the following commands
% >> cd 'C:\Users\Huang Zuli\scoop\lib_source\signals\ltfat'
%% or >> cd '~/proj/ml_matlab/external_sources/ltfat' in personal mac 
% >> ltfatstart

function y = imdctWindowed(x, lFrame, varargin)
    % The dimension of x is (N, 1)
    N = size(x, 1); 
    % define the sine bell window function

    % determine if user specifies the used window
    temp = find(strcmp(varargin,'winname') == 1); % search for window name
    if isempty(temp)                            % if not given
        winname = 'sine';                       % default is sine window
    else                                        % if user supplies frequency
        winname = varargin{temp+1};             % assign to user input
        varargin(temp:temp+1) = [];             % remove from varargin
    end
    clear temp

    sineWin = wilwin(winname, lFrame);
    [y, Ls] = iwmdct(x, sineWin); 
end
