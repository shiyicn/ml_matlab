% To use the toolbox, 'cd' to the LTFAT directory and execute 'ltfatstart'
% Using the following commands
%{
if ispc
    cd 'C:\\Users\\Huang Zuli\\scoop\\lib_source\\signals\\ltfat'
else
    cd '~/proj/lib_sources/ltfat'
end
ltfatstart
%}
% End

function y = mdctWindowed(x, lFrame, varargin)
    %{
    Implement the method that facilitate the usage of MDCT
    
    
    
    %}
    % The dimension of x is (N, 1)
    N = size(x, 1); 
    % define the sine bell window function
    % determine if user specified frequency
    temp = find(strcmp(varargin,'winname') == 1); % search for window name
    if isempty(temp)                            % if not given
        winname = 'sine';                       % default is sine window
    else                                        % if user supplies frequency
        winname = varargin{temp+1};             % assign to user input
        varargin(temp:temp+1) = [];             % remove from varargin
    end
    clear temp
    fprintf('Window name is {%s}.\n', winname)
    sineWin = wilwin(winname, lFrame);
    [y, Ls] = wmdct(x, sineWin, lFrame); 
end
