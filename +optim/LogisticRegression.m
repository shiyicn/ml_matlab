classdef LogisticRegression

    properties (PropertyAttributes)
        pen
        w
        w0
    end

    methods (MethodAttributes)
        function obj = methodName(obj, pen)
            obj.pen = pen;
            obj.w0 = 0;
        end
    end

end