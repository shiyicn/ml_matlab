% basic linear algebra functions

function U = gram_schmit(V)
    % gram_schmit - Description
    %
    % Syntax: U = gram_schmit(V)
    %
    % Long description
    % 

    n = size(V, 1);
    k = size(V, 2);
    U = zeros(n, k);
    % normalise the first column vector
    U(:, 1) = V(:, 1) / norm(V(:, 1));
    for i = 2:k
        U(:, i) = V(:, i);
        for j = 1:(i-1)
            U(:, i) = U(:, i) - (U(:, i)' * U(:, j)) ...
                    / (U(:, j)' * U(:, j)) * U(:, j);
        end
        % normalise the column vector i
        U(:, i) = U(:, i) / norm(U(:, i));
    end
end

