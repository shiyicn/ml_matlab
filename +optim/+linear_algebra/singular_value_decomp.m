function [U, S, V] = singular_value_decomp(M)
    % svd - Description
    % Singular Value Decomposition 
    % Any matrix M can be decomposed to : M = U*S*V 
    %
    % Syntax: [U, V, S] = svd(M)
    %
    % Long description

    [V, S] = eig(M' * M);
    [U, _] = eig(M * M');
end
