function [XNorm, mu, deviation] = normalize(X)
    if length(size(X))
        fprintf('Dimension of X is %d, not a 2-dim matrix.\n', length(size(X)));
    end
    % by default by column
    mu = mean(X, 1);
    deviation = std(X, 0, 1); % by default a un-biased estimator is used

    XNorm = (X - mu) ./ deviation;
end
