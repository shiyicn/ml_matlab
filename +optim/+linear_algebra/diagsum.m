function dsum = diagsum(A)
    dsum = fliplr(sum(spdiags(A)));
end