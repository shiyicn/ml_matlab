function [UL, UR, BL, BR] = md4p(X)
    % md4p - Description
    %
    % Syntax: [UL, UR, BL, BR] = md4p(X)
    %
    % Long description
    [m, n] = size(X);
    midCol = n / 2;
    midRow = m / 2;
    UL = X(1:midRow, 1:midCol);
    UR = X(1:midRow, (midCol + 1):n);
    BL = X((midRow + 1):m, 1:midCol);
    BR = X((midRow + 1):m, (midCol+1):n);
end