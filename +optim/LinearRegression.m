% Multiple Useful functions for linear models

% Load linear algebra package
% addpath('~/proj/ml_matlab/')
% load as optim.LinearRegression

classdef LinearRegression < handle

	properties
		pen
		w
		w0
	end

	methods

		function obj = LinearRegression(pen)
			% ================================================================
			% Function: 
			% ---------
			% Create an object containing main linear regression methods

			% Args: 
			% -----
			% pen : numerical value, ridge penalization coefficient
			
			% Returns:
			% --------
			% obj : LinearRegression object
			% ================================================================
			if nargin > 0
				obj.pen = pen;
			end
			obj.w0 = 0;
		end

		function [w, w0] = fit(obj, X, y, varargin)
			% ================================================================
			% Function: 
			% ---------
			% Find the Linear Regression coefficients between X and y
			% Fit the Linear Regression Model via the normal equation
			% w in argmin || X*w - y ||^2 + 1/2 * pen * ||w||^2
			% w = (X' * X + pen)^-1 * X' * y

			% Example:
			% >> lr = LinearRegression(0)
			% >> X = rand(100, 10)
			% >> y = rand(100, 1)
			% >> [w, w0] = lr.fit(X, y)
			% >> disp(w), disp(w0)
			% >> disp(lr)

			% Args: 
			% -----
			% X : input matrix, shape (N, p)
			% y : output vector, shape (N, 1)
			
			% Returns:
			% --------
			% w : linear regression coefficient, shape (N, 1)
			% w0 : bias term, numerical value
			% ================================================================
			
			temp = find(strcmp(varargin, 'intercept') == 1); % search for const flag
			if isempty(temp)                             % if not given
				interceptFlag = false;                   % default is false
			else                                        % if user supplies flag
				interceptFlag = varargin{temp+1};        % assign to user input
				varargin(temp:temp+1) = [];              % remove from varargin
			end
			clear temp

			[N, p] = size(X); % compute the dimensions

			if interceptFlag
				X = [ones(N, 1) X];
			end
			w_extended = (X' * X) \ (X' * y);
			if interceptFlag
				w0 = w_extended(1);
				w = w_extended(2:p+1);
			else
				w0 = 0;
				w = w_extended;
			end
			obj.w0 = w0;
			obj.w = w;
		end

		function [w, w0] = successive_fit(obj, X, y)
			[N, p] = size(X);
			w = zeros(p, 1);
			w0 = y * w / sqrt(n);
			residuals = y - w0;
			for i = 1:p
				w(i) = residuals * X(:, i) / sqrt(norm(X(:, i)));
				residuals = residuals - w(i) * X(:, i);
			end
			obj.w = w;
			obj.w0 = w0;
		end
		
		function [w, w0, Z] = orthogonal_fit(obj, X, y, varargin)
			[N, p] = size(X);
			w = zeros(p, 1);
			if varargin ~= 0 || varargin == 'const'
				X = [ones(N, 1) X];
			end
			% start to orthogonalise the input matrix X to Z
			Z = gram_schmit(X);
			w_extended = (Z' * Z) * Z' * y;
			if varargin ~= 0 || varargin == 'const'
				w0 = w_extended(1);
				w = w_extended(2:p+1);
			end
			obj.w0 = w0;
			obj.w = w;
		end

		function [z_stat, pvalue] = confidenceInterval(obj, X, y)
			% ================================================================
			% Function: 
			% ---------
			% zero-test for regression coefficients
			% statistical analysis of the coefficients from linear regression
			% using the independence between the coeff and variance, it follows a 
			% student distribution 
			%                   wj
			% Zj = ----------------------------  ~ t(N-p-1)
			%       sqrt(variance) * (X' * X)jj

			% Args: 
			% -----
			% X : input matrix, shape (N, p)
			% y : output vector, shape (N, 1)
			
			% Returns:
			% --------
			% z_stat : Z-statistics for each coefficients, shape (N, 1)
			% pvalue : p-value for each coeff zero test, shape (N, 1)			
			% ================================================================
			
			[N, p] = size(X);
			residuals = X * obj.w + obj.w0 - y;
			variance = 1. / (N - p - 1) * sum(residuals.^2);
			z_stat = obj.w ./ (sqrt(variance) * diag(X.' * X));
			pvalue = 2 * (1 - tcdf(abs(z_stat), N-p-1));
		end

		function res = residuals(obj, X, y)
			res = X * obj.w + obj.w0 - y;
		end

		function [coef, mu, deviation] = lars(obj, X, y, pen)
			p = size(X, 2);
			coef = zeros(p, 1);
			XOriginal = X;
			% Step 1: Standardize the predictors
			[X, mu, deviation] = optim.linear_algebra.normalize(XOriginal);
			YOriginal = y;
			numEnteredCoef = 0;
			while numEnteredCoef ~= p
				residuals = y - mean(y);
				correlations = X' *residuals;
				[argvalue, argmax] = max(abs(correlations));
				
				coef(argmax) = argvalue;
			end
		end

		function glmnet(obj, X, y, pen)
		end
	end
end
