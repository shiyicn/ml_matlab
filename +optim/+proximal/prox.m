function X = prox(Y, q, pen)
    %{
    ================================================================
    Function: 
    ---------
    compute the proximal argmin for the following optim problem

    prox(Y) = 1/2 || X - Y ||^2 + pen * ||Y||^q

    Args: 
    -----
    Y : input matrix like argument
    pen : numerical value, ridge penalization coefficient
    q : the degree the of norm for the penalisation

    Example:
    --------
    >> Y = rand(100, 100)
    >> X = prox(Y, 2, 0.1)
    
    Returns:
    --------
    X : the proximal projection
    ================================================================
    %}
    max_iter = 10;
    sz = size(Y);

    % for q == 1 or 2, we have the explicit solution
    if (q == 2)
        X = Y / (1 + 2 * pen);
    elseif (q == 1)
        X = sign(Y) .* max(abs(Y) - pen, 0);
    else
        % use the newton's method to solve the equation
        X = 0.5 * ones(sz);
        Y_abs = abs(Y);
        for k = 1:max_iter
            % in order to compute the solution f(X) = 0
            % Xn+1 = Xn - f(Xn) / f'(Xn)
            X = X - (q * pen * X.^(q-1) + X - Y_abs) ./ ...
                (q * (q-1) * pen * X.^(q-2) + 1);
            % for the stability of the algorithm, since we only consider positive solution, we threshold the iteration by positive number
            X = max(X, 0);
        end
        X = X .* sign(Y);
    end
end
