% addpath('~/proj/ml_matlab/optim/proximal/')
function imgDenoised = wd(imgNoised, pen, q, level, wname)
    %{
    ================================================================
    Function: 
    ---------
    denoise an image with wavelet approximation operator and proximal operatioin

    prox_f (y) = 1/2 || x - y ||^2 + f(Ly)
    
    where L is the discrete wavelet operator R^N -> R^N and f(x) = 0 if x is approximation coefficient, or pen * ||x||_q if x is detail coefficient. 

    Args: 
    -----
    imgNoised : the noised image to be processed
    pen : numerical value, ridge penalization coefficient
    q : the degree the of norm for the penalisation
    level : wavelet transform leve
    wname : the type of wavelet to be used

    Example:
    --------
    >> img = rand(100, 100)
    >> imgDenoised = wd(imgNoised, 0.2, 1, 4, 'sym4');
    
    Returns:
    --------
    imgDenoised : the denoised output image
    ================================================================
    %}

    [C, S] = wavedec2(imgNoised, level, wname);
    % compute proximal projection
    imgProximal = prox(C, q, pen);
    approxRange = prod(S(1, :));
    imgProximal(1:approxRange) = C(1:approxRange); % recover approximation
    imgDenoised = waverec2(imgProximal, S, wname); 
end
