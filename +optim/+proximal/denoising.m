if ispc
    imgPath = 'C:\\Users\\Huang Zuli\\proj\\data\\img_example\\florence.jpg';
else
    imgPath = '~/proj/ml_matlab/data/img_example/florence.jpg';
end

if ispc
    addpath('C:\\Users\\Huang Zuli\\proj\\ml_matlab')
else
    addpath('~/proj/ml_matlab/')
end

if ispc
    dataRootPath = 'C:\\Users\\Huang Zuli\\proj\\data\\votes';
else
    dataRootPath = '~/proj/data/votes/';
end

if ispc
    outputPath = 'C:\\Users\\Huang Zuli\\proj\\data\\votes\\demo_results';
else
    outputPath = '~/proj/data/votes/demo_results';
end

img = imread(imgPath);
img = im2double(img);

IMG_RANGE = 255;
sigmaNoise = 30 / IMG_RANGE;

sz = size(img);

imgNoised = img + normrnd(0, sigmaNoise, sz);

%{
% Using denoising method of Wavelet
imwrite(imgNoised, 'noised_img.jpg')

for q = [1, 4/3, 3/2, 2, 3, 4]
    for pen = [0.01, 0.1, 0.125, 0.2, 1]
        imgDenoised = wd(imgNoised, pen, q, 4, 'sym4');
        fprintf('save file : norm-%d-pen-%.3f.jpg\n', q, pen);
        imwrite(imgDenoised, sprintf('norm-%d-pen-%.3f.jpg', q, pen))
    end
end
%}

y = imgNoised(:);
N = eye(size(y, 1));
solver = signalProcessing.MaximumEntropyDenoising(y);

