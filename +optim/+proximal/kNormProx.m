function v = kNormProx(x, inGamma, K, y)
    N = size(x, 1) % x is an n-colum vector
    v = (eye(N) + inGamma * K.' * K) \ ...
         (x + inGamma * K.' * y)
end