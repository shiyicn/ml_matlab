function v = entProx(x, inGamma)
    v = inGamma * lambertw(1. / inGamma * exp(x / inGamma - 1.));
end