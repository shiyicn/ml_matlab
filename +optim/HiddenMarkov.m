classdef (ClassAttributes) HiddenMarkov

    properties (PropertyAttributes)
        X_train
        y_train
    end

    methods (MethodAttributes)
        function obj = HiddenMarkov(obj, args)
            
        end

        function [likelihood] = fit(obj, X_train, y_train)
            
        end

        function [alpha] = alpha_rec(obj, X_train, y_train)
        end

        function [beta] = beta_rec(obj, X_train, y_train)
        end

        function [likelihood] = viterbi(obj, X_train, y_train)
        end
        
        function predict(obj, X)
        end
        
        function eval(obj, X, y)
        end
    end
    
end