#include <iostream>
#include <random>

class AbstractArm {
    public:
        virtual double sample() = 0;
};

class BernoulliArm: public AbstractArm {
    public:
        double p;
        std::bernoulli_distribution dist;
        std::default_random_engine gen;

        double sample() {
            return this->dist(this->gen);
        }
        
        BernoulliArm(double p) {
            this->p = p;
            // initialize generator
            this->dist = std::bernoulli_distribution(this->p);
        }
};

