/*
def UCB1(T, mab, rho = 1):
    """
    
    Params:
    -------
    T : integer 
        the horizon of time 
    mab : a list of arms 
        represented as multi-arms bandit
        an arm must have a function sample to generate a reward
    
    Output:
    -------
    A series of decisions
    
    """

    K = len(mab) # num of arms

    if T <= K:
        print('Not enough simulations!')

    def update_bound(Ns, delta, mus_hat):
        return mus_hat + \
                rho * np.sqrt(np.log(1. / delta) / (2 * Ns))
    
    # count the times that an arm is triggered
    Ns = np.ones(K) # an arm should be trigerred at lease once
    mus_hat = np.zeros(K)
    Bs = np.zeros(K)

    history_reward = []
    history_arm = []

    for i in range(K):
        history_arm.append(i)
        reward = mab[i].sample()
        mus_hat[i] = reward
        history_reward.append(reward)
    
    update_bound(Ns, 1./K, mus_hat)

    for t in range(K, T):
        a_t = random_argmax(Bs)
        history_arm.append(a_t)
        reward = mab[a_t].sample()
        history_reward.append(reward)
        mus_hat[a_t] = (reward + Ns[a_t] * mus_hat[a_t]) / (Ns[a_t] + 1.)
        Ns[a_t] += 1
        Bs = update_bound(Ns, 1./t, mus_hat)
        #Bs[a_t] = B(a_t, Ns[a_t], 1. / t, mus_hat[a_t])
    
    return history_reward, history_arm
*/

#include "mex.h"
#include <stdio.h>
#include <iostream>
#include <string>
#include <random>

using namespace std;

void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[]) {
    /**
     * A problem here : 
     * It seems that the pseudo random numbers has always the same seed
     * 
     */

    #define T_IN prhs[0]
    #define MAB prhs[1]
    #define N_IN prhs[2]

    #define X_OUT plhs[0]
    
    double mu = *mxGetPr(MU_IN);
    double sigma = *mxGetPr(SIGMA_IN);
    int N = (int) *mxGetPr(N_IN);

    X_OUT = mxCreateDoubleMatrix(1, N, mxREAL); /* Create the output matrix */
    double* X = mxGetPr(X_OUT);


}